# Contributing

First of all, thank you for considering contributing to Kerbal Future. We're a small team - only five people at the moment - so we can and will appreciate all the help we can get.

Following these guidelines tells us that you respect our time. In turn, we'll reciprocate that respect by addressing your issue in a timely and friendly fashion.

Kerbal Future is an open source project and we welcome contributions from the open source community. There are many ways to contribute; tutorials, documentation, bug reports - and, of course, writing code to be integrated into Kerbal Future itself.

Please don't use the issue tracker for support, questions, or other miscellany. The official IRC channel (#kerbaeadastra on Freenode) can help you with your issue.

## Ground Rules

There is only one important ground rule, and it is as follows:

> Be welcoming to newcomers and encourage diverse new contributors from all backgrounds. See our [Code of Conduct](https://github.com/KerbaeAdAstra/KerbalFuture/blob/community-standards-update/CODE_OF_CONDUCT.md).

## Contributing

* Each pull request should implement one, and only one, feature or bug fix. Should you wish to implement or fix more than one feature/bug, submit more than one pull request.
* The commits of each pull request should be directly relevant to the pull request itself. Do not commit changes to files that are irrelevant to the pull request.
* Do not add `using` directives that point to third-party libraries, unless said library is essential to the project. When in doubt, contact the project maintainers.
* Be willing to accept constructive criticism from the project maintainers.
* Be aware that the pull request review process is not immediate, and that the project maintainers have other things to do. Do not pester the project maintainers.
* Low-effort pull requests will generally be rejected.

## Bug Reporting

### Security Vulnerabilities

If you find a security vulnerability, **DO NOT** open an issue! Send an email to kerbaeadastra@gmail.com. If you're a PGP user, feel free to encrypt to `A00A 5B66 377B EC69 5C4B  8CE8 A7B8 2AD1 4B50 74A3`.

### Bug Reporting Etiquette

To make our work easier, please answer these questions when submitting a bug report:
1. What version of Kerbal Space Program is the mod running on?
2. What version is the mod itself?
3. What operating system and processor architecture are you using?
4. What did you do that caused this bug?
5. What exceptions were thrown?

And most importantly, you **must** include the KSP.log (preferably in GitHub Gist form) when submitting a bug report. **NO LOGS = NO SUPPORT.**

## Suggesting Features

Kerbal Future is based on a foundation of hard science-fiction. Handwaving is kept to a minimum. Kerbal Future will implement most if not all paradigms of Kerbae ad Astra - see the official Wiki [here](https://github.com/KerbaeAdAstra/wiki/wiki). As such, features requests or suggestions will probably be denied unless it fits in with the universe.

## Community

You can reach the team in several ways. The preferred way is of course IRC - the channel is #kerbaeadastra on Freenode. Alternatively, send an email to kerbaeadastra@gmail.com entitled "To [TEAMMEMBER]".
