# KerbalFuture

## Licensing

The part configuration files (\*.cfg) and C# source files (\*.cs) [henceforth the Software] are copyright (c) the Kerbae ad Astra group, 2017, and distributed under the MIT License <<https://opensource.org/licenses/MIT>>. You are free to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, given that this copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

Portions of the plugin code has been taken from Jay2645's Randomized-KSP-Systems. This code is copyright (c) Jay2645, 2014-2017, and licensed under the GNU General Public License, Version 2 <<https://opensource.org/licenses/GPL-2.0>>. The original repository may be accessed at <<https://github.com/Jay2645/Randomized-KSP-Systems>>. The code is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License, Version 2, as published by the Free Software Foundation.

The art assets (i.e. all textures, meshes, and models) are copyright (c) 0111narwhalz, 2017, and distributed under the Creative Commons Attribution-NonCommercial-ShareAlike license <<https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode>>. You are free to share and adapt the materials only for non-commercial purposes and when providing appropriate attribution. Any derivatives must be distributed under the same (or a less restrictive) license.
