# Kerbae ad Astra Code of Conduct
The Kerbal Future group wishes to extend the warmest welcome to any and all contributors as possible. In order to accomplish this, we ask the following things from you.
## IRC Channel
When visiting the Kerbae ad Astra IRC channel (#kerbaeadastra) on Freenode, accessible through http://webchat.freenode.net, we ask that you be respectful and thoughtful before posting. Any abuse will not be tolerated, and reports can and will be made to the staff members of the channel. This includes the following:
* Sexual or suggestive language
* Links to inappropriate pictures or websites. As a rule-of-thumb, if the KSP Forums would block it, so will we.
* Posting of inappropriate pictures. Using certain clients, it is possible to post pictures directly in chat. See above for the rule-of-thumb.
* Abusive language of any kind, insulting others in any way. "Playful" teasing is not tolerated, nor is any other form of disrespect for other members.
* Role playing while on the channel is not acceptable.
* Comments about any other user's suspected race, sexual orientation, or ethnicity is not tolerated. As a rule of thumb, if you feel as though others will make fun of you for revealing your race or ethnicity, do not state it in the channel. The same goes for sexual orientation.
* Keep all posts on topic, and for any confidential information, personal message the user.
* Political statements of any kind will not be tolerated.
* Posting other users' personal information (doxxing) is forbidden.
* To restate all of the above, keep posts on topic and SFW [Safe For Work], and respect other user's beliefs and practices.
## GitHub
When visiting the Kerbae ad Astra GitHub page, found at github.com/KerbaeAdAstra, you have the ability of commenting on others commits, issues, and pull requests (PRs) if you have a GitHub account. These comments should be kept on topic and shall not make personal attacks or condescending statements. These include:
* Making a request to close said issue or PR because "you are not experiencing the problem".
* Arguing about the content of issues or PRs, either created by yourself or others.
* Trying to justify issues or PRs after they have been closed by Kerbae ad Astra staff.
* Making issues or PRs without a purpose.
* Unhelpful creation of issues such as "it is completely broken". These do not help anyone in any way, and create much more unnecessary communication and waiting time for the issue to be fixed.
## The Staff Members of Kerbae ad Astra
* TotallyNotHuman\_
* 0111narwhalz
* KAL 9000
* Benjamin Kerman
* Brent Kerman

We (the staff members) are committed to keeping Kerbae ad Astra and all related places healthy and accepting for all users.
## Consequences for misconduct
### IRC Channel
The consequences for misconduct are available on the IRC channel by typing the command '/msg ChanServ taxonomy #kerbaeadastra' in the chat bar.
### GitHub
Misconduct will result in blocking and reporting to GitHub staff, and removal of any and all inappropriate wiki edits, commits, comments, issues, and pull requests.
### KSP Forums, including any and all Kerbae ad Astra mantained threads
An immediate report to the KSP forum moderators will be made - punishments (or the lack thereof) will be decided by them, seeing as the Forums are under their jurisdiction.
## Reports of Abuse
We take abuse very seriously. If you would like to make a complaint of abuse or harassment, please send us an email at kerbaeadastra@gmail.com. We will review each case thoroughly, and may invite you into a private session to discuss the incident. All findings will be kept confidential, unless otherwise stated or requested.
## Thank you for visiting Kerbae ad Astra, and enjoy your stay!
